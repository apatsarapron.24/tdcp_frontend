import Vue from "vue";
import IdleVue from "idle-vue";
import "./plugins/vuetify";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import axios from "axios";
import VueAxios from "vue-axios";
import VueCookies from "vue-cookies";
import TransactionModal from "./components/transactionmodal.vue";
import invoiceofTransaction from "./components/InvoiceofTransaction";
import exportmodal from "./components/Exportmodal";
import VueSimpleAlert from "vue-simple-alert";
import VueSweetAlert from "vue-sweetalert";
import PrimeVue from "primevue/config";
import ScrollPanel from "primevue/scrollpanel";
import VueApexCharts from "vue-apexcharts";

Vue.use(VueSimpleAlert);
Vue.use(VueAxios, axios);
Vue.use(VueCookies);
Vue.use(VueSweetAlert);
Vue.use(PrimeVue);
Vue.use(VueApexCharts);

VueCookies.config("7d");
Vue.component("Transactionmodal", TransactionModal);
Vue.component("InvoiceofTransaction", invoiceofTransaction);
Vue.component("Exportmodal", exportmodal);
Vue.component("ScrollPanel", ScrollPanel);
Vue.component("apexchart", VueApexCharts);
Vue.config.productionTip = false;

const eventsHub = new Vue();

Vue.use(IdleVue, {
  eventEmitter: eventsHub,
  store,
  idleTime: 30000000, // 50 minutes,
  // idleTime: 300000, // 5 minutes,
  startAtIdle: false
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
