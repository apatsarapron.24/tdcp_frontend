import Vue from "vue";
import Router from "vue-router";
import VueSession from "vue-session";

// layout
import Login from "@/layouts/Login";
import LoginAdmin from "@/layouts/LoginAdmin";
import Dashboard from "@/layouts/Dashboard";

// views
import PageNotFound from "@/views/PageNotFound.vue";
// import Unpaidbill from '@/views/Unpaidbill.vue'
import Unpaidbill from "@/views/Unpaidbill.vue";
import Paidbill from "@/views/Paidbill.vue";
import Transaction from "@/views/Transaction.vue";
import TransactionResult from "@/views/TransactionResult.vue";
import PaymentStep from "@/views/PaymentStep.vue";
import Checkout from "@/views/Checkout.vue";
import PaidSuccess from "@/views/PaidSuccess.vue";
import CustomerManagement from "@/views/CustomerManagement.vue";
import UserManagement from "@/views/UserManagement.vue";
import Noinvoice from "@/views/Noinvoice.vue";
// import WithholdingTax from '@/views/WithholdingTax.vue'
import Home from "@/views/Home.vue";
import UploadBill from "@/views/Uploadbill.vue";
import "./components/css/style.css";
import ExternalCheckout from "@/views/ExternalCheckout.vue";
import PayTax from "@/views/Paytax.vue";
import Sandbox from "@/views/Sandbox.vue";
import SandboxEwht from "@/views/SandboxEwht.vue";
import SandboxRD from "@/views/SandboxRD.vue";
import SandboxRDEwht from "@/views/SandboxRDEwht.vue";
import MainDashboard from "@/views/MainDashboard.vue";
import RiskAssessment from "@/views/RiskAssessment.vue";

Vue.use(Router);
Vue.use(VueSession);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "login",
      component: Login
    },
    {
      path: "/admin",
      name: "LoginAdmin",
      component: LoginAdmin
    },
    // eslint-disable-next-line no-undef
    { path: "*", name: "pageNotFound", component: PageNotFound },
    {
      path: "/transactionResult",
      name: "transactionResult",
      component: TransactionResult
    },
    // {
    //   path: '/checkout',
    //   name: 'checkout',
    //   component: Checkout
    // },
    // {
    //   path: '/summarybills',
    //   name: 'summarybills',
    //   component: PaymentStep
    // },
    {
      path: "/warning",
      name: "noinvoice",
      component: Noinvoice
    },
    {
      path: "/paidsuccess",
      name: "PaidSuccess",
      component: PaidSuccess
    },
    {
      path: "/qrcheckout",
      name: "qrcheckout",
      component: ExternalCheckout
    },
    {
      path: "/sandbox",
      name: "sandbox",
      component: Sandbox
    },
    {
      path: "/sandboxewht",
      name: "sandboxewht",
      component: SandboxEwht
    },
    {
      path: "/sandboxrd",
      name: "sandboxrd",
      component: SandboxRD
    },
    {
      path: "/sandboxrdewht",
      name: "sandboxrdewht",
      component: SandboxRDEwht
    },
    {
      path: "/",
      name: "dashboard",
      component: Dashboard,
      children: [
        {
          path: "/home",
          name: "home",
          component: Home
        },
        {
          path: "unpaidbill",
          name: "unpaidbill",
          component: Unpaidbill
        },
        {
          path: "paidbill",
          name: "paidbill",
          component: Paidbill
        },
        {
          path: "transaction",
          name: "transaction",
          component: Transaction
        },
        // {
        //   path: 'about',
        //   name: 'about',
        //   component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
        // },
        {
          path: "customermanagement",
          name: "customermanagement",
          component: CustomerManagement
        },
        // {
        //   path: 'withholdingtax',
        //   name: 'withholdingtax',
        //   component: WithholdingTax
        // },
        {
          path: "uploadbill",
          name: "uploadbill",
          component: UploadBill
        },
        {
          path: "adminmanagement",
          name: "adminmanagement",
          component: UserManagement
        },
        {
          path: "/checkout",
          name: "checkout",
          component: Checkout
        },
        {
          path: "/summarybills",
          name: "summarybills",
          component: PaymentStep
        },
        {
          path: "/paytax",
          name: "paytax",
          component: PayTax
        },
        {
          path: "/maindashboard",
          name: "maindashboard",
          component: MainDashboard
        },
        {
          path: "/riskassessment",
          name: "riskassessment",
          component: RiskAssessment
        }
      ]
    }
  ]
});
