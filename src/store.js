import Vue from 'vue'
import Vuex from 'vuex'
import pathify, { make } from 'vuex-pathify'

const state = {
  invoicemodal: false,
  currentinvoiceno: '',
  currenttotalpay: [],
  totalpricepay: '',
  transactionmodal: false,
  currenttransaction: '',
  invoiceoftransaction: false,
  exportmodal: '',
  totalExcVat: '',
  totalvatamount: '',
  invoicename: '',
  invoicetotal: '',
  invoicedata: [],
  tokenHashTaxid: '',
  exportfrom: '',
  imageCompany: '',
  selectedcpn: ''
}

const mutations = make.mutations(state)

Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [
    pathify.plugin
  ],
  state,
  mutations
})
